'use strict'

import React, { Component } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import Counter from '../components/Counter'
import { incrementCounter, decrementCounter } from '../actions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import * as counterActions from '../actions';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

class CounterApp extends Component {
    constructor(props) {
        super(props);
    }
    
    render() {
        const { state, actions } = this.props;
        console.log(actions);
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>
                    Sample Counter
                </Text>
                <Counter 
                    value={state.value}
                    {...actions} />
            </View>
        );
    }
}

export default connect(state => ({
        state: state
    }), 
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(CounterApp);