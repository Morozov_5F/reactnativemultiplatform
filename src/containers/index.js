import React, { Component } from 'react';

import { createStore } from 'redux' 
import { Provider } from 'react-redux'

import counter from '../reducers'
import CounterApp from '../containers/CounterApp'

const store = createStore(counter);

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <CounterApp />
            </Provider>
        );
    }
}