import { INCREMENT, DECREMENT } from '../constants/ActionTypes'

export const incrementCounter = () => ({
    type: INCREMENT
})

export const decrementCounter = () => ({
    type: DECREMENT
})