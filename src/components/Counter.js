import React, { Component } from 'react'
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native'

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

class Counter extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { value, incrementCounter, decrementCounter } = this.props;
        
        return (
            <View style={styles.container}>
                <Text>Value: {value} </Text>
                <TouchableOpacity onPress={incrementCounter}>
                    <Text style={styles.instructions}>INCREMENT</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={decrementCounter}>
                    <Text style={styles.instructions}>DECREMENT</Text>
                </TouchableOpacity>
            </View>
        );
    }
} 

export default Counter